import { ISocketHandlerFactory } from "../socket/interfaces";

import {
  AddRoomHandler,
  GameOverHandler,
  InputCharHandler,
  JoinRoomHandler,
  LeaveRoomBeforeGameHandler,
  LeaveRoomDuringGameHandler,
  LeaveRoomHandler,
  StartGameHandler,
  TogglePlayerStateHandler,
  UserConnectHandler,
  UserDisconnectHandler,
} from "./handlers";

import KeyboardRacingHandlerType from "./KeyboardRacingHandlerType";

//FACTORY
class KeyboardRacingHandlerFactory implements ISocketHandlerFactory {
  public create(type: KeyboardRacingHandlerType) {
    switch (type) {
      case KeyboardRacingHandlerType.USER_CONNECT:
        return new UserConnectHandler();
      case KeyboardRacingHandlerType.ADD_ROOM:
        return new AddRoomHandler();
      case KeyboardRacingHandlerType.JOIN_TO_ROOM:
        return new JoinRoomHandler();
      case KeyboardRacingHandlerType.TOGGLE_PLAYER_STATE:
        return new TogglePlayerStateHandler();
      case KeyboardRacingHandlerType.ROOM_EXIT:
        return new LeaveRoomHandler();
      case KeyboardRacingHandlerType.INPUT_CHAR:
        return new InputCharHandler();
      case KeyboardRacingHandlerType.USER_DISCONNECT:
        return new UserDisconnectHandler();
      case KeyboardRacingHandlerType.START_GAME:
        return new StartGameHandler();
      case KeyboardRacingHandlerType.GAME_OVER:
        return new GameOverHandler();
      case KeyboardRacingHandlerType.LEAVE_ROOM_BEFORE_GAME:
        return new LeaveRoomBeforeGameHandler();
      case KeyboardRacingHandlerType.LEAVE_ROOM_DURING_GAME:
        return new LeaveRoomDuringGameHandler();
      default:
        return null;
    }
  }
}

export default KeyboardRacingHandlerFactory;
