import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { getGameResults } from "../../services/gameService/gameSimpleService";
import { clearRoom } from "../../services/roomService/roomSimpleService";
import { mapRoomForClient } from "../../helpers/room.helper";
import { getOneRoom } from "../../services/roomService/roomCrudService";

class GameOverHandler implements ISocketHandler {
  handle({ io, param: roomName }: ISocketHandlerOptions): void {
    if (!io || !roomName) {
      return;
    }

    const room = getOneRoom(roomName);

    if (!room) {
      return;
    }

    io.to(roomName).emit("GAME_IS_OVER", getGameResults(room));

    const cleanRoom = clearRoom(room);

    if (!cleanRoom) {
      return;
    }

    io.to(roomName).emit("UPDATE_ROOM", mapRoomForClient(cleanRoom));
  }
}

export default GameOverHandler;
