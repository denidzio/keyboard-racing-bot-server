import { mapRoomForClient } from "../../helpers/room.helper";
import { removeRoom } from "../../services/roomService/roomCrudService";
import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

class LeaveRoomDuringGameHandler implements ISocketHandler {
  handle({ socket, param: { room, user } }: ISocketHandlerOptions): void {
    if (!socket || !room || !user) {
      return;
    }

    if (room.players.length === 0) {
      if (room.timerId) {
        clearTimeout(room.timerId);
      }

      removeRoom(room.name);
      return;
    }

    socket.to(room.name).emit("UPDATE_ROOM", mapRoomForClient(room));
  }
}

export default LeaveRoomDuringGameHandler;
