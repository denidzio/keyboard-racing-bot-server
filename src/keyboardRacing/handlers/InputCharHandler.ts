import { texts } from "../../data";

import { ISocketHandler, ISocketHandlerOptions } from "../../socket/interfaces";

import { didAllPlayersComplete } from "../../services/gameService/gameSimpleService";
import { getPlayerByRoomAndUser } from "../../services/playerService/playerSimpleService";
import {
  clearRoomTimeout,
  getRoomWithUser,
  isAllowedToInputChar,
  isRoomInGame,
  updatePlayerInRoom,
} from "../../services/roomService/roomSimpleService";
import { getOneUser } from "../../services/userService/userCrudService";

import KeyboardRacingHandlerFactory from "../KeyboardRacingHandlerFactory";
import KeyboardRacingHandlerType from "../KeyboardRacingHandlerType";

import { mapRoomForClient } from "../../helpers/room.helper";
import { Player } from "../../entities";

class InputCharHandler implements ISocketHandler {
  public handle({ io, socket, param: char }: ISocketHandlerOptions): void {
    if (!io || !socket || !char) {
      return;
    }

    const user = getOneUser(socket.handshake.query.username as string);

    if (!user) {
      return;
    }

    const room = getRoomWithUser(user);

    if (!room || !isRoomInGame(room) || !isAllowedToInputChar(room)) {
      return;
    }

    const player = getPlayerByRoomAndUser(room, user);

    if (
      !player ||
      player.inputtedText === undefined ||
      room.textId === undefined
    ) {
      return;
    }

    const text = texts[room.textId];

    if (char !== text[player.inputtedText.length]) {
      return;
    }

    const updatedPlayer = new Player({
      ...player,
      inputtedText: player.inputtedText + char,
    });

    updatedPlayer.progress =
      (100 * updatedPlayer.inputtedText.length) / text.length;

    if (updatedPlayer.progress === 100) {
      updatedPlayer.finishTime = Date.now();
    }

    const updatedRoom = updatePlayerInRoom(room.name, updatedPlayer);

    if (!updatedRoom) {
      return;
    }

    io.to(room.name).emit("UPDATE_ROOM", mapRoomForClient(updatedRoom));

    if (didAllPlayersComplete(updatedRoom)) {
      const handlerFactory = new KeyboardRacingHandlerFactory();

      const gameOverHandler = handlerFactory.create(
        KeyboardRacingHandlerType.GAME_OVER
      );

      gameOverHandler?.handle({ io, param: room.name });
      clearRoomTimeout(room);
    }
  }
}

export default InputCharHandler;
