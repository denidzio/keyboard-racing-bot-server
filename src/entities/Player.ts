import { IUser, IPlayer } from "./interfaces";

class Player implements IPlayer {
  public user: IUser;
  public isReady: boolean = false;
  public inputtedText: string = "";
  public progress: number = 0;
  public finishTime?: number | undefined;

  constructor({ user, isReady, inputtedText, progress, finishTime }: IPlayer) {
    this.user = user;

    if (isReady) {
      this.isReady = isReady;
    }

    if (inputtedText) {
      this.inputtedText = inputtedText;
    }

    if (progress) {
      this.progress = progress;
    }

    if (finishTime) {
      this.finishTime = finishTime;
    }
  }
}

export default Player;
