import { IPlayer, IRoom } from "./interfaces";

class Room implements IRoom {
  public name: string = "Simple room";
  public players: IPlayer[] = [];
  public hasBeenStartedAt?: number | undefined;
  public textId?: number | undefined;
  public timerId?: NodeJS.Timeout | undefined;

  constructor({ name, players, hasBeenStartedAt, textId, timerId }: IRoom) {
    this.name = name;
    this.players = players;
    this.hasBeenStartedAt = hasBeenStartedAt;
    this.textId = textId;
    this.timerId = timerId;
  }
}

export default Room;
