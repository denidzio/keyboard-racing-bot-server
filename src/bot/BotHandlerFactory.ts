import { ISocketHandlerFactory } from "../socket/interfaces";
import BotHandlerType from "./BotHandlerType";
import {
  DuringGameHandler,
  EndGameHandler,
  StartGameHandler,
  StatsHandler,
} from "./handlers";

//FACTORY
class BotHandlerFactory implements ISocketHandlerFactory {
  public create(type: BotHandlerType) {
    switch (type) {
      case BotHandlerType.DURING_GAME:
        return new DuringGameHandler();
      case BotHandlerType.END_GAME:
        return new EndGameHandler();
      case BotHandlerType.START_GAME:
        return new StartGameHandler();
      case BotHandlerType.STATS:
        return new StatsHandler();
      default:
        return null;
    }
  }
}

export default BotHandlerFactory;
