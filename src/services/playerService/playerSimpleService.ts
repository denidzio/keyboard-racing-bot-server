import { SECOND } from "../../constants";
import { IPlayer, IRoom, IUser } from "../../entities/interfaces";
import { SECONDS_FOR_GAME } from "../../socket/config";
import { getOneRoom } from "../roomService/roomCrudService";

export const getPlayerByUser = (user: IUser | undefined) => {
  if (!user || !user.currentRoomName) {
    return;
  }

  const userRoom = getOneRoom(user.currentRoomName);

  if (!userRoom) {
    return;
  }

  return userRoom.players.find(
    (player) => player.user.username === user.username
  );
};

export const getPlayerByRoomAndUser = (
  room: IRoom | undefined,
  user: IUser | undefined
) => {
  if (!room || !user) {
    return;
  }

  return room.players.find((p) => p.user.username === user.username);
};

export const getCleanPlayer = (player: IPlayer | undefined) => {
  if (!player) {
    return;
  }

  return {
    user: player.user,
    isReady: false,
    inputtedText: "",
    progress: 0,
  };
};

export const getCleanPlayers = (players: IPlayer[] | undefined) => {
  if (!players) {
    return;
  }

  return players.reduce((acc, player) => {
    const cleanPlayer = getCleanPlayer(player);

    if (!cleanPlayer) {
      return acc;
    }

    return [...acc, cleanPlayer];
  }, <IPlayer[]>[]);
};

export const getPlayersAsString = (players: IPlayer[] | undefined) => {
  if (!players) {
    return;
  }

  return players
    .reduce(
      (acc, player, index) =>
        `${acc} ${index + 1}. ${
          player.user.username
        } (${player.progress?.toFixed(0)}%)\n`,
      ""
    )
    .trim();
};

export const getPlayerTime = (
  room: IRoom | undefined,
  player: IPlayer | undefined
) => {
  if (!room || !player || !room.hasBeenStartedAt) {
    return 0;
  }

  if (!player.finishTime) {
    return SECONDS_FOR_GAME * SECOND;
  }

  return player.finishTime - room.hasBeenStartedAt;
};
