import { IRoom } from "../../entities/interfaces";

export const getGameResults = (room: IRoom | undefined) => {
  if (!room) {
    return;
  }

  const players = room.players;

  if (players.find((player) => player.progress === undefined)) {
    return;
  }

  return players.sort((a, b) => {
    if (!a.finishTime && !b.finishTime) {
      return (b.progress as number) - (a.progress as number);
    }

    if (!a.finishTime && b.finishTime) {
      return 1;
    }

    if (a.finishTime && !b.finishTime) {
      return -1;
    }

    if (a.finishTime && b.finishTime) {
      return a.finishTime - b.finishTime;
    }

    return 0;
  });
};

export const didAllPlayersComplete = (room: IRoom | undefined) => {
  if (!room) {
    return;
  }

  return room.players.every((p) => p.progress === 100);
};

export const isProgressChanged = (
  oldRoom: IRoom | undefined,
  newRoom: IRoom | undefined,
  playerName: string | undefined
): boolean => {
  if (!oldRoom || !newRoom || !playerName) {
    return false;
  }

  const playerFromOldRoom = oldRoom.players.find(
    (player) => player.user.username === playerName
  );
  const playerFromNewRoom = newRoom.players.find(
    (player) => player.user.username === playerName
  );

  if (!playerFromOldRoom || !playerFromNewRoom) {
    return false;
  }

  if (playerFromOldRoom.progress === playerFromNewRoom.progress) {
    return false;
  }

  return true;
};
