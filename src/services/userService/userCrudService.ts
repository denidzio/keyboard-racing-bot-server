import { getAll, create, getOne, update, remove } from "../../dao/userDao";
import { IUser } from "../../entities/interfaces";
import { isFieldUniq } from "../../helpers/array.helper";

export const getAllUsers = () => {
  return getAll();
};

export const createUser = (user: IUser | undefined) => {
  if (!user) {
    return;
  }

  //CURRYING
  if (!isFieldUniq(getAll())("username")(user.username)) {
    return;
  }

  return create(user);
};

export const getOneUser = (username: string | undefined) => {
  if (!username) {
    return;
  }

  return getOne(username);
};

export const updateUser = (user: IUser | undefined) => {
  if (!user) {
    return;
  }

  return update(user);
};

export const removeUser = (username: string | undefined) => {
  if (!username) {
    return;
  }

  return remove(username);
};
