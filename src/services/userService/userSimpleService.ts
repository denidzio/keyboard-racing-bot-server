import { IUser } from "../../entities/interfaces";

export const doesUserCouldJoinToRoom = (user: IUser): boolean => {
  return !user.currentRoomName;
};
